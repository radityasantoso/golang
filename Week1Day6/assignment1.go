package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

type Worker struct {
	IdKaryawan int
	Nama       string
}

type Staff struct {
	Jabatan string
	Worker
}

func InputWorker(staff *[]Staff) {

	var id int
	var nama string
	var jabatan string

	fmt.Print("Enter id: ")
	fmt.Scan(&id)

	fmt.Print("Enter name: ")
	fmt.Scan(&nama)

	fmt.Print("Masukkan Jabatan Staff: ")
	fmt.Scan(&jabatan)

	staffNew := Staff{}
	staffNew.IdKaryawan = id
	staffNew.Nama = nama
	staffNew.Jabatan = jabatan

	*staff = append(*staff, staffNew)
}

func showStaff(staff []Staff, timeInSecs int64) {
	fmt.Println("")
	fmt.Println(strings.Repeat("_", 33))
	fmt.Println("| Id | |Nama\t |Jabatan\t|")
	fmt.Println(strings.Repeat("_", 33))

	for _, el := range staff {
		fmt.Printf("| %d  |", el.IdKaryawan)

		fmt.Printf(" |%s\t", el.Nama)

		fmt.Printf(" |%s\t|", el.Jabatan)

		fmt.Println("")

		time.Sleep(time.Duration(timeInSecs) * 1e9)

	}
}

func main() {
	var staff []Staff

	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. New Staff")
		fmt.Println("2. Laporan Staff")
		fmt.Println("3. Exit")
		fmt.Printf("Masukan pilihan: ")

		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		if userInput == 3 {

			fmt.Println("=======EXIT=======")
			break
		}
		switch userInput {
		case 1:
			InputWorker(&staff)
		case 2:

			go showStaff(staff, 3)
		}
	}

}
