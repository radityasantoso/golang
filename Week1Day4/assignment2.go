package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
)

type Mahasiswa struct {
	IdMhs   int64  `json:"idMhs,omitempty" validate:"max=10,min=1"`
	NamaMhs string `json:"namaMhs,omitempty"`
	Nilai   []int
}

// type Nilai struct {
// 	Fisika   int64  `json:"nilaiFisika,omitempty"`
// 	Biologi    int64  `json:"nilaiBiologi,omitempty"`
// 	Kimia        int64 `json:"nilaiKimia,omitempty"`
// }

func main() {

	var mahasiswa []Mahasiswa
	scanner := bufio.NewScanner(os.Stdin)
	for {
		//scanner := bufio.NewScanner(os.Stdin)

		//var Input int
		fmt.Println("Menu")
		fmt.Println("1.Input Data Mahasiswa")
		fmt.Println("2.Hitung Gaji Staff")
		fmt.Println("3.Tampilkan Data Staff")
		fmt.Println("4.EXIT")
		fmt.Println("====================")
		// condition to terminate the loop
		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		Input, _ := strconv.ParseInt(scanner.Text(), 10, 64)

		if Input == 4 {
			fmt.Printf("Program Exited")
			break
		}

		fmt.Println("====================")

		switch Input {
		case 1:
			inputMahasiswa(&mahasiswa)
		case 2:
			seleksi(&mahasiswa)
		case 3:
			fmt.Println("== Tampil Data Staff : ==")
			for _, el := range mahasiswa {
				printStaff(el)
				fmt.Println("")
			}

		default:
			fmt.Println("====EXIT====")

		}
	}
}
func inputMahasiswa(mahasiswa *[]Mahasiswa) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Input Data Mahasiswa")
	fmt.Println("________________________")

	fmt.Print("Masukkan Id Mahasiswa: ")
	scanner.Scan()
	id, _ := strconv.ParseInt(scanner.Text(), 10, 64)

	fmt.Print("Masukkan Nama Mahasiswa: ")
	scanner.Scan()
	nama := scanner.Text()

	size := 3
	array := make([]int, size)
	for i := 0; i < size; i++ {
		fmt.Printf("Enter Nilai Mahasiwa index ke %d: ", i)
		fmt.Scan(&array[i])
	}

	var Data Mahasiswa
	Data = Mahasiswa{
		IdMhs:   id,
		NamaMhs: nama,
		Nilai:   array,
	}
	*mahasiswa = append(*mahasiswa, Data)
	fmt.Println(Data)
}

func seleksi(mahasiswa *[]Mahasiswa) {
	for i, el := range *mahasiswa {
		if (el.Nilai[0]+el.Nilai[1]+el.Nilai[2])/3 > 70 {
			// (*mahasiswa)[i].Status := "Lulus"
			fmt.Println(stringify((*mahasiswa)[i]))
			fmt.Println("Lulus")
		} else {
			// (*mahasiswa)[i].Status := "Tidak Lulus"
			fmt.Println(stringify((*mahasiswa)[i]))
			fmt.Println("Tidak Lulus")
		}

	}
}
func stringify(data interface{}) string {
	b, _ := json.MarshalIndent(data, " ", " ")
	return string(b)
}

func printStaff(mahasiswa Mahasiswa) {
	fmt.Println(stringify(mahasiswa))
}
