package main

import "fmt"

//Shaper is an interface and has a single function Area that returns an int.
type Shaper interface {
	Area() int
}
type Rectangle struct {
	length, width int
}

//this function Area works on the type Rectangle and has the same function signature
//defined in the interface Shaper. Therefore, Rectangle now implements the interface Shaper.
func (r Rectangle) Area() int {
	return r.length * r.width
}

func main() {
	r := Rectangle{length: 5, width: 3}
	fmt.Println("Rectangle r details are: ", r)
	fmt.Println("Rectangle r's area is: ", r.Area())
	s := Shaper(r)
	fmt.Println("Area of the Shape r is: ", s.Area())
}
