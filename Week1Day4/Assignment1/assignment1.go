package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
)

type Staff struct {
	IdStaff      int64  //`json:"idStaff,omitempty" validate:"max=10,min=1"`
	NamaStaff    string //`json:"namaStaff,omitempty"`
	JabatanStaff string //`json:"jabatanStaff,omitempty"`
	GapokStaff   int64  //`json:"gapokStaff,omitempty"`
	TunjStaff    int64  //`json:"tunjStaff,omitempty"`
	TGaji        int64
}

// type Book struct {
// 	Id      int    `json:"book_id,omitempty" validate:"max=10,min=1"`
// 	Author  Person `json:"author,omitempty"`
// 	Subject string `json:"subject,omitempty"`
// 	Title   string `json:"title,omitempty"`
// 	Status  bool
// }

func main() {

	var staff []Staff
	scanner := bufio.NewScanner(os.Stdin)
	for {
		//scanner := bufio.NewScanner(os.Stdin)

		//var Input int
		fmt.Println("Menu")
		fmt.Println("1.Input Data Staff")
		fmt.Println("2.Hitung Gaji Staff")
		fmt.Println("3.Tampilkan Data Staff")
		fmt.Println("4.EXIT")
		fmt.Println("====================")
		// condition to terminate the loop
		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		Input, _ := strconv.ParseInt(scanner.Text(), 10, 64)

		if Input == 4 {
			fmt.Printf("Program Exited")
			break
		}

		fmt.Println("====================")

		switch Input {
		case 1:
			inputStaf(&staff)
		case 2:

			hitungGaji(&staff)
		case 3:
			fmt.Println("== Tampil Data Staff : ==")
			for _, el := range staff {
				printStaff(el)
				fmt.Println("")
			}

		default:
			fmt.Println("====EXIT====")

		}
	}
}
func inputStaf(staff *[]Staff) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Input Data Staff")
	fmt.Println("________________________")

	fmt.Print("Masukkan Id Staff: ")
	scanner.Scan()
	id, _ := strconv.ParseInt(scanner.Text(), 10, 64)

	fmt.Print("Masukkan Nama Staff: ")
	scanner.Scan()
	nama := scanner.Text()

	fmt.Print("Masukkan Jabatan Staff: ")
	scanner.Scan()
	jabatan := scanner.Text()

	fmt.Print("Masukkan Gaji Pokok Staff: ")
	scanner.Scan()
	gapok, _ := strconv.ParseInt(scanner.Text(), 10, 64)

	fmt.Print("Masukkan Tunjangan Staff: ")
	scanner.Scan()
	tunj, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	var Data Staff
	Data = Staff{
		IdStaff:      id,
		NamaStaff:    nama,
		JabatanStaff: jabatan,
		GapokStaff:   gapok,
		TunjStaff:    tunj,
		TGaji:        Data.GapokStaff + Data.TunjStaff,
	}
	*staff = append(*staff, Data)
	fmt.Println(Data)
}
func hitungGaji(staff *[]Staff) {
	for i, el := range *staff {
		fmt.Printf("hitung total gaji %s \n", el.NamaStaff)
		(*staff)[i].TGaji = el.GapokStaff + el.TunjStaff
	}
}
func stringify(data interface{}) string {
	b, _ := json.MarshalIndent(data, " ", " ")
	return string(b)
}

func printStaff(staff Staff) {
	fmt.Println(stringify(staff))
}
