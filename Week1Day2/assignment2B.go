package main

import "fmt"

func main() {
	var segitiga string
	var i int
	var j int

	for i = 3; i > 0; i-- {
		for j = 1; j <= i; j++ {
			if j >= i {
				segitiga += "*"
				fmt.Printf(segitiga)
			} else {

				fmt.Printf(" ")
			}
		}

		fmt.Printf("\n")
	}

}
