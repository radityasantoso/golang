package main

import "fmt"

func main() {
	var kendaraan string = ""
	var roda int = 2

	switch roda {
	case 2:
		kendaraan = "Motor"
	case 4:
		kendaraan = "Mobil"
	case 5, 6, 7, 8, 9, 10:
		kendaraan = "Truk atau Bus"
	}
	fmt.Printf(kendaraan)

}
