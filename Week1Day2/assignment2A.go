package main

import "fmt"

func main() {
	var i int

	for i = 0; i < 9; {
		i++
		plus := 1 + i

		fmt.Printf("1 + %d = %d \n", i, plus)

	}

}
