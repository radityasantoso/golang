package main

import "fmt"

func main() {

	// loop that runs infinitely
	for {
		var input int
		fmt.Println("MENU")
		fmt.Println("=================")
		fmt.Println("1. Luas Segitiga")
		fmt.Println("2. Luas Lingkaran")
		fmt.Println("3. Luas Persegi")
		fmt.Println("=================")
		fmt.Println("4. EXIT")
		fmt.Print("Masukan pilihan : ")
		fmt.Scan(&input)
		// condition to terminate the loop
		if input >= 4 {
			fmt.Println("======EXIT=======")
			break
		}

		switch input {
		case 1:
			fmt.Println("Menghitung Luas Segitiga")
			var alas float32
			var tinggi float32
			var luas_segitiga float32
			fmt.Print("Masukan alas = ")
			fmt.Scan(&alas)
			fmt.Print("Masukan tinggi = ")
			fmt.Scan(&tinggi)
			luas_segitiga = alas * tinggi / 2
			fmt.Printf("Luas Segitiga adalah = %.2f\n", luas_segitiga)
		case 2:
			fmt.Println("Menghitung Luas Lingkaran")
			const phi = 3.14
			var jari2 float32
			var luas_lingkaran float32
			fmt.Print("Masukan Jari-jari = ")
			fmt.Scan(&jari2)
			luas_lingkaran = phi * jari2 * jari2
			fmt.Printf("Luas Lingkaran adalah = %.2f\n", luas_lingkaran)
		case 3:
			fmt.Println("Menghitung Luas Persegi")
			var panjang float32
			var lebar float32
			var luas_persegi float32
			fmt.Print("Masukan panjang = ")
			fmt.Scan(&panjang)
			fmt.Print("Masukan lebar = ")
			fmt.Scan(&lebar)
			luas_persegi = panjang * lebar
			fmt.Printf("Luas Persegi adalah = %.2f\n", luas_persegi)
		}

	}
}
