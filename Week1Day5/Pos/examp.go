package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"time"
)

type History interface {
	// MyHistory(IdProduct int, NameProduct string, PriceProduct int, StockProduct int)
	// Total() int

}

type Product struct {
	IdProduct    int64
	NameProduct  string
	PriceProduct int64
	StockProduct int64
}
type Report struct {
	IdR   int64
	Date  string
	Total int64
}

// func hitungGaji(product *[]Product) {
// 	for i, el := range *product {
// 		fmt.Printf("hitung total gaji %s \n", el.NamaStaff)
// 		(*staff)[i].TGaji = el.GapokStaff + el.TunjStaff
// 	}
// }
// func (pr Product) MyHistory(IdProduct int, NameProduct string, PriceProduct int, StockProduct int){
// 	return pr.IdProduct, pr.NameProduct, pr.PriceProduct, pr.StockProduct
// }

type Transaction struct {
	No       int64
	Name     string
	Qty      int64
	Subtotal int64
}

func main() {
	var grandtotal int64
	var product []Product
	var report []Report
	product = append(product, Product{IdProduct: 1, NameProduct: "Buku", PriceProduct: 10000, StockProduct: 30})
	product = append(product, Product{IdProduct: 2, NameProduct: "Pulpen", PriceProduct: 5000, StockProduct: 50})
	product = append(product, Product{IdProduct: 3, NameProduct: "Pensil", PriceProduct: 4000, StockProduct: 50})
	product = append(product, Product{IdProduct: 4, NameProduct: "Penghapus", PriceProduct: 2000, StockProduct: 40})
	product = append(product, Product{IdProduct: 5, NameProduct: "Penggaris", PriceProduct: 8000, StockProduct: 15})
	// fmt.Println("== Tampil Data Staff : ==")
	// for _, el := range product {
	// 	printStaff(el)
	// 	fmt.Println("")
	// }
	scanner := bufio.NewScanner(os.Stdin)
	var transaction []Transaction
	for {
		//scanner := bufio.NewScanner(os.Stdin)

		//var Input int
		fmt.Println("================Menu==================")
		fmt.Println("1.New Transaction")
		fmt.Println("2.Transaction Report")
		fmt.Println("3.List Product")
		fmt.Println("4.EXIT")
		fmt.Println("======================================")
		// condition to terminate the loop
		fmt.Printf("Masukan Input Menu: ")
		scanner.Scan()
		Input, _ := strconv.ParseInt(scanner.Text(), 10, 64)

		if Input == 4 {
			fmt.Printf("Program Exited")
			break
		}

		fmt.Println("====================")

		switch Input {
		case 1:
			for {
				//scanner := bufio.NewScanner(os.Stdin)

				//var Input int
				fmt.Println("================Menu==================")
				fmt.Println("1.Add more items")
				fmt.Println("2.See Product List")
				fmt.Println("3.Payment")
				fmt.Println("4.EXIT")
				fmt.Println("======================================")
				// condition to terminate the loop
				fmt.Printf("Masukan Input Menu: ")
				scanner.Scan()
				Pilih, _ := strconv.ParseInt(scanner.Text(), 10, 64)
				fmt.Println("====================")

				if Pilih == 1 {
					scanner := bufio.NewScanner(os.Stdin)
					fmt.Print("Masukkan Id Product: ")
					scanner.Scan()
					id, _ := strconv.ParseInt(scanner.Text(), 10, 64)

					fmt.Print("Masukkan Qty Product: ")
					scanner.Scan()
					qty, _ := strconv.ParseInt(scanner.Text(), 10, 64)
					Transaksi(&product, id, qty)

					no := int64(len(transaction)) + 1
					name := FindName(&product, id)
					subtotal := Subtotal(&product, id, qty)
					var Data Transaction
					Data = Transaction{
						No:       no,
						Name:     name,
						Qty:      qty,
						Subtotal: subtotal,
					}
					//transaction [el]Transaction
					transaction = append(transaction, Data)
					fmt.Println(transaction)
					ShowTransaksi(&transaction)
					grandtotal += Data.Subtotal
					// fmt.Println(grantotal)
					// for _, el := range transaction {
					// 	printTransaction(el)
					// 	fmt.Println("")
					// }
					// for _, el := range transaction {
					// 	printTransaction(el)
					// 	fmt.Println("")
					// }
				} else if Pilih == 2 {
					ShowTransaksi(&transaction)
				} else if Pilih == 3 {
					fmt.Println("BILL\t:", grandtotal)
					fmt.Print("PAID \t: ")
					scanner.Scan()
					paid, _ := strconv.ParseInt(scanner.Text(), 10, 64)

					for {
						if paid < int64(grandtotal) {
							fmt.Print("PAID \t: ")
							scanner.Scan()
							paid, _ = strconv.ParseInt(scanner.Text(), 10, 64)
						} else {
							break
						}
					}

					fmt.Println("Change \t:", int64(paid)-grandtotal)
					t := time.Now()
					idreport := len(report) + 1

					//var Datar Report
					var Datar = Report{
						IdR:   int64(idreport),
						Date:  t.Format("2006-01-02 15:04:05"),
						Total: grandtotal,
					}
					report = append(report, Datar)

					// fmt.Print("Press anyting to continue")
					// scanner.Scan()
					// // fmt.Println(stringify(transactions))
					// // clear transaction
					// grandTotal = 0
					// itemTransactions = nil
					break
				}
			}

		case 2:
			ShowReport(&report)

		case 3:
			fmt.Println("======== List Product ========")
			fmt.Println("Id\t Nama\t Price\t Stock")
			for _, el := range product {
				fmt.Printf("%d\t %s\t %d\t %d \n", el.IdProduct, el.NameProduct, el.PriceProduct, el.StockProduct)

			}

		default:
			fmt.Println("====EXIT====")

		}
	}

	// var transaction []Transaction

}
func Transaksi(product *[]Product, id int64, qty int64) {

	for i, el := range *product {

		if el.IdProduct == id {

			(*product)[i].StockProduct = el.StockProduct - qty

		}

	}

}

func FindName(product *[]Product, id int64) string {
	var nama string
	for _, el := range *product {
		if el.IdProduct == id {
			nama = el.NameProduct

		}

	}
	return nama
}
func ShowTransaksi(transaction *[]Transaction) {
	fmt.Println("No\t Nama\t Qty\t SubTotal")
	var grandtotal int64
	for _, el := range *transaction {
		fmt.Printf("%d\t %s\t %d\t %d \n", el.No, el.Name, el.Qty, el.Subtotal)

		grandtotal += el.Subtotal

		//fmt.Println("Grand Total :", grandtotal)
		// fmt.Printf(transaction)
		// printTransaction(el)
		// fmt.Println("")

	}
	fmt.Println("==========================")
	fmt.Println("Grand Total :", grandtotal)

}

func ShowReport(report *[]Report) {
	fmt.Println("Id\t Date\t Total")
	for _, el := range *report {
		fmt.Printf("%d\t %s\t %d \n", el.IdR, el.Date, el.Total)

		//fmt.Println("Grand Total :", grandtotal)
		// fmt.Printf(transaction)
		// printTransaction(el)
		// fmt.Println("")

	}

}
func Subtotal(product *[]Product, id int64, qty int64) int64 {
	var total int64
	for _, el := range *product {
		if el.IdProduct == id {
			total = el.PriceProduct * qty

		}

	}
	return total
}

// func Transaksi(product *[]Product){
// 	var transaction []Transaction
// 	scanner := bufio.NewScanner(os.Stdin)
// 	fmt.Print("Masukkan Id Product: ")
// 	scanner.Scan()
// 	id, _ := strconv.ParseInt(scanner.Text(), 10, 64)

// 	fmt.Print("Masukkan Qty Product: ")
// 	scanner.Scan()
// 	qty, _ := strconv.ParseInt(scanner.Text(), 10, 64)
// 	for _, el := range *product {
// 		if (el.IdProduct) == id {
// no := int64(len(transaction)) + 1
// name := el.NameProduct
// subtotal := el.PriceProduct * qty
// var Data Transaction
// Data = Transaction{
// 	No:       no,
// 	Name:     name,
// 	Qty:      qty,
// 	Subtotal: subtotal,
// }
// //transaction [el]Transaction
// transaction = append(transaction, Data)
// 			fmt.Println(transaction)
// 			// fmt.Println(stringify((*product)[i]))
// 			// fmt.Println(subtotal)
// 			// fmt.Println("Lulus")
// 		}

// 	}
// 	return transaction
// }

func stringify(data interface{}) string {
	b, _ := json.MarshalIndent(data, " ", "\t")
	return string(b)
}

func printProduct(product Product) {
	fmt.Println(stringify(product))
}
func printTransaction(transaction Transaction) {
	fmt.Println(stringify(transaction))
}
