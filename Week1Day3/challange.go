package main

import (
	"fmt"
)

func getAverage(arr []float32) float32 {
	var i int

	var avg, sum float32
	for i = 0; i < len(arr); i++ {
		sum += arr[i]
	}
	avg = sum / float32(len(arr))
	return avg

}

// func myfun(a [6]int, size int) int {
// 	var k, val, r int

// 	for k = 0; k < size; k++ {
// 		val += a[k]
// 	}

// 	r = val / size
// 	return r
// }

// Main function
// func main() {

//     // Creating and initializing an array
//     var arr = [6]int{67, 59, 29, 35, 4, 34}
//     var res int

//     // Passing an array as an argument
//     res = myfun(arr, 6)
//     fmt.Printf("Final result is: %d ", res)
// }

// Main function
func main() {

	// Creating and initializing an array
	var arr = []float32{6.7, 5.9, 2.9, 3.5, 4.0, 3.4}
	res := getAverage(arr)

	fmt.Printf("Final result is: %d ", res)
}
