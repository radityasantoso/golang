package main

import "fmt"

func main() {

	// loop that runs infinitely
	for {
		var input int
		fmt.Println("MENU")
		fmt.Println("=================")
		fmt.Println("1. Array 1 Dimensi")
		fmt.Println("2. Array 2 Dimensi")
		fmt.Println("=================")
		fmt.Println("4. EXIT")
		fmt.Print("Masukan pilihan : ")
		fmt.Scan(&input)
		// condition to terminate the loop
		if input >= 4 {
			fmt.Println("======EXIT=======")
			break
		}

		switch input {
		case 1:
			var size int

			fmt.Print("Enter the size of array: ")
			fmt.Scan(&size)
			fmt.Print("Enter the array elements: ")
			array := make([]int, size)

			for i := 0; i < size; i++ {
				fmt.Scan(&array[i])

			}
			fmt.Println(array)
			for index, value := range array {
				fmt.Printf("index %d of value = %d\n", index, value)
			}
		case 2:

			// var matrix2[100][100] int
			// var sum[100][100] int
			var row, col int
			fmt.Print("Enter number of rows: ")
			fmt.Scan(&row)
			fmt.Print("Enter number of cols: ")
			fmt.Scan(&col)
			var matrix1 [100][100]int

			fmt.Println()
			fmt.Println("========== Matrix1 =============")
			fmt.Println()
			for i := 0; i < row; i++ {
				for j := 0; j < col; j++ {
					fmt.Printf("Enter the element for Matrix1 %d %d :", i+1, j+1)
					fmt.Scan(&matrix1[i][j])
				}
			}
			fmt.Println("========== Show of Matrix =============")
			fmt.Println()

			for i := 0; i < row; i++ {
				for j := 0; j < col; j++ {
					fmt.Printf(" %d ", matrix1[i][j])
					if j == col-1 {
						fmt.Println("")
					}
				}
			}
		default:
			fmt.Println("====EXIT====")

		}

	}
}
