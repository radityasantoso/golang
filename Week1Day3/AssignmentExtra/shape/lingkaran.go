package shape

import (
	"math"
)

type Lingkaran struct {
	Jari float64
}

func (ling Lingkaran) GetLuas() float64 {
	var luas_lingkaran float64
	luas_lingkaran = math.Phi * ling.Jari * ling.Jari

	return luas_lingkaran
}
