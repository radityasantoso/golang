package shape

//"math"

type Segitiga struct {
	Alas   float64
	Tinggi float64
}

func (seg Segitiga) GetLuas() float64 {
	var luas_segitiga float64
	luas_segitiga = seg.Alas * seg.Tinggi / 2

	return luas_segitiga
}
