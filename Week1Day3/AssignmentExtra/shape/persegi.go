package shape

//"math"

type Persegi struct {
	Panjang float64
	Lebar   float64
}

func (per Persegi) GetLuas() float64 {
	var luas_persegi float64
	luas_persegi = per.Panjang * per.Lebar

	return luas_persegi
}
