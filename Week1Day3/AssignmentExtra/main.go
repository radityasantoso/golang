package main

import (
	"fmt"
	"radit/shape"
)

func main() {

	// loop that runs infinitely
	for {
		var input int
		fmt.Println("MENU")
		fmt.Println("=================")
		fmt.Println("1. Luas Segitiga")
		fmt.Println("2. Luas Lingkaran")
		fmt.Println("3. Luas Persegi")
		fmt.Println("=================")
		fmt.Println("4. EXIT")
		fmt.Print("Masukan pilihan : ")
		fmt.Scan(&input)
		// condition to terminate the loop
		if input >= 4 {
			fmt.Println("======EXIT=======")
			break
		}

		switch input {
		case 1:
			fmt.Println("Menghitung Luas Segitiga")
			var alas float64
			var tinggi float64
			//var segitiga float64
			fmt.Print("Masukan alas = ")
			fmt.Scan(&alas)
			fmt.Print("Masukan tinggi = ")
			fmt.Scan(&tinggi)
			segitiga := shape.Segitiga{
				Alas:   alas,
				Tinggi: tinggi,
			}
			fmt.Printf("Luas Segitiga adalah = %.2f\n", segitiga.GetLuas())
		case 2:
			fmt.Println("Menghitung Luas Lingkaran")
			//const phi = 3.14
			var jari2 float64

			fmt.Print("Masukan Jari-jari = ")
			fmt.Scan(&jari2)
			lingkaran := shape.Lingkaran{
				Jari: jari2,
			}
			fmt.Printf("Luas Lingkaran adalah = %.2f\n", lingkaran.GetLuas())
		case 3:
			fmt.Println("Menghitung Luas Persegi")
			var panjang float64
			var lebar float64
			//var persegi float32
			fmt.Print("Masukan panjang = ")
			fmt.Scan(&panjang)
			fmt.Print("Masukan lebar = ")
			fmt.Scan(&lebar)
			persegi := shape.Persegi{
				Panjang: panjang,
				Lebar:   lebar,
			}
			fmt.Printf("Luas Persegi adalah = %.2f\n", persegi.GetLuas())
		}

	}
}
