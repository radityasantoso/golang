package controller

type Kubus struct {
	Panjang float32
	Lebar   float32
	Tinggi  float32
}

func (k Kubus) GetVolumeKubus() float32 {
	return k.Panjang * k.Tinggi * k.Lebar
}

type Bola struct {
	Jari2 float32
}

func (b Bola) GetVolumeBola() float32 {
	const phi = 3.14
	return 4 * phi * b.Jari2 * b.Jari2 * b.Jari2 / 3
}

type Umur struct {
	Thn_Sekarang int
	Thn_Lahir    int
}

func (u Umur) GetUmur() int {
	return u.Thn_Sekarang - u.Thn_Lahir
}
