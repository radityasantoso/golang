package main

import (
	"fmt"
	"raditya/controller"
)

func main() {

	// loop that runs infinitely
	for {
		var input int
		fmt.Println("==================")
		fmt.Println("----->MENU<-------")
		fmt.Println(" -----------------")
		fmt.Println("|1. Volume Kubus |")
		fmt.Println(" -----------------")
		fmt.Println("|2. Volume Bola|")
		fmt.Println(" -----------------")
		fmt.Println("|3. Hitung Umur  |")
		fmt.Println(" -----------------")
		fmt.Println("|4. EXIT		  |")
		fmt.Println(" -----------------")
		fmt.Println("==================")
		fmt.Print("Masukan pilihan : ")
		fmt.Scan(&input)
		// condition to terminate the loop
		if input >= 4 {
			fmt.Println("----------------------------")
			fmt.Println("----- Have A Nice Day ------")
			fmt.Println("----------------------------")
			break
		}

		switch input {
		case 1:
			fmt.Println("Menghitung Volume Kubus")
			var p float32
			var l float32
			var t float32
			// var luas_segitiga float32
			fmt.Print("Masukan panjang = ")
			fmt.Scan(&p)
			fmt.Print("Masukan lebar = ")
			fmt.Scan(&l)
			fmt.Print("Masukan tinggi = ")
			fmt.Scan(&t)

			kubus := controller.Kubus{
				Panjang: p,
				Lebar:   l,
				Tinggi:  t,
			}

			fmt.Printf("Volume Kubus = %.1f\n", kubus.GetVolumeKubus())
		case 2:
			fmt.Println("Menghitung Volume Bola")
			var j float32
			fmt.Print("Masukan Jari-jari = ")
			fmt.Scan(&j)

			bola := controller.Bola{
				Jari2: j,
			}

			fmt.Printf("Volume Bola adalah = %.1f\n", bola.GetVolumeBola())
		case 3:
			fmt.Println("Menghitung Umur Anda")
			var now int
			var born int
			fmt.Print("Masukan Tahun Sekarang = ")
			fmt.Scan(&now)
			fmt.Print("Masukan Tahun Lahir = ")
			fmt.Scan(&born)

			umur := controller.Umur{
				Thn_Sekarang: now,
				Thn_Lahir:    born,
			}

			fmt.Println("Umur anda adalah = ", umur.GetUmur(), "tahun")
		}

	}
}
