package main

import "fmt"

func main() {

	var i = 5
	var j float64 = -6.6

	fmt.Printf("i is of type %T \n", i)
	fmt.Printf("value of i is: %d at address %p", i, &i)

	fmt.Printf("j is of type %T \n", j)
	fmt.Printf("value of j is: %.1f at address %p", j, &j)

}
